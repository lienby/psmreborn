function copy_text(zrif)
{
	window.prompt("Press CTRL+C to copy",zrif);
}

function open_url(url)
{
	window.location = url;
}

function pkg_change_version()
{
	var VerSelect = document.getElementsByClassName("version-select")[0];
	var PkgSony = document.getElementById("pkg-sony");
	var PkgPsmReborn = document.getElementById("pkg-psmrb");
	var Ver = VerSelect.value;
	
	/*
	* Sony URL Change
	*/
	SonyUrl = PkgSony.getElementsByTagName('a')[0].href;
	var UrlSplit = SonyUrl.split('/')
	UrlSplit[7] = Ver
	var PatchedUrl = UrlSplit.join("/")
	PkgSony.outerHTML = PkgSony.outerHTML.replace(SonyUrl,PatchedUrl)
	document.getElementById('pkg-sony').getElementsByTagName('a')[0].href = PatchedUrl;  //change it again because chrome is stupid
	
	/*
	* PSM Reborn URL Change
	*/
	
	var PsmRbUrl = PkgPsmReborn.getElementsByTagName('a')[0].href;
	UrlSplit = PsmRbUrl.split('/')
	UrlSplit[5] = Ver
	PatchedUrl = UrlSplit.join("/")
	PkgPsmReborn.outerHTML = PkgPsmReborn.outerHTML.replace(PsmRbUrl,PatchedUrl)
	document.getElementById('pkg-psmrb').getElementsByTagName('a')[0].href = PatchedUrl;  //change it again because chrome is stupid
	
	//Also you cant acccess it with variables, it just doesnt update in the browser (it does if u use console though.. because javascript lol)
}