<?php
function update_rifs()
{
	$npsTime = filemtime("NpsPsm.tsv");
	$npsPendingTime = filemtime("NpsPendingPsm.tsv");


	if(time() > $npsTime + 86400)
	{
		rename("NpsPsm.tsv","nps-backup/NpsPsm_".strval($npsTime).".tsv");
		rename("NpsPendingPsm.tsv","nps-backup/NpsPendingPsm_".strval($npsPendingTime).".tsv");
		file_put_contents("NpsPsm.tsv", file_get_contents("http://nopaystation.com/tsv/PSM_GAMES.tsv"));
		file_put_contents("NpsPendingPsm.tsv", file_get_contents("https://nopaystation.com/tsv/pending/PSM_GAMES.tsv"));
	}
}
update_rifs();

if(strcmp($_SERVER['HTTP_HOST'],"psmreborn.com") !== 0)
{
	if(strcmp($_SERVER['HTTP_HOST'],"psm.cbps.xyz") !== 0)
	{
		die("Invalid request! HOST: ".$_SERVER['HTTP_HOST']);
	}
}

function getTitle(string $game)
{
	$xml = simplexml_load_file('gameinfo/' . $game . "/app.xml", 'SimpleXMLElement', LIBXML_NOENT);
	$title = $xml->name->localized_item[0]->attributes()->value;
	unset($xml);
	return $title;
}

function getPlayableList()
{
	$playable_list = (array)null;
	$delimiter = "\t";
	$fp = fopen("NpsPsm.tsv", 'r');
	while (!feof($fp))
	{
		$line = fgets($fp, 2048);
		$data = str_getcsv($line, $delimiter);
		$playable = 0;
		if($data[4] != "MISSING")
		{
			$playable = 1;
		}
		$playable_list[$data[0]] = $playable;
	
		
	}                              
	fclose($fp);
	return $playable_list;
}


function getPendingPlayableList()
{
	$playable_list = (array)null;
	$delimiter = "\t";
	$fp = fopen("NpsPendingPsm.tsv", 'r');
	while (!feof($fp))
	{
		$line = fgets($fp, 2048);
		$data = str_getcsv($line, $delimiter);
		$playable = 0;
		if($data[4] != "MISSING")
		{
			$playable = 1;
		}
		
		$gameTid = basename($data[3], "_00.pkg");
		$playable_list[$gameTid] = $playable;
	
		
	}                              
	fclose($fp);
	return $playable_list;
}


function getZRIF(string $titleid)
{
	$delimiter = "\t";

	$fp = fopen("NpsPsm.tsv", 'r');
	// Workaround for NPS having multiple entries for some reason
	
	$zrif = "MISSING";
	
	while ( !feof($fp) )
	{
		$line = fgets($fp, 2048);

		$data = str_getcsv($line, $delimiter);

		if($data[0] == $titleid)
		{
			$zrif = $data[4];
		}
	}           
	
	fclose($fp);
	return $zrif;
}

function getPendingZRIF(string $titleid)
{
	$delimiter = "\t";

	$fp = fopen("NpsPendingPsm.tsv", 'r');

	while ( !feof($fp) )
	{
		$line = fgets($fp, 2048);

		$data = str_getcsv($line, $delimiter);
		$gameTid = basename($data[3], "_00.pkg");
		if($gameTid == $titleid)
		{
			return($data[4]);
		}
	}                              

	fclose($fp);
	return "MISSING";
}

function getPKG(string $titleid)
{
	$delimiter = "\t";

	$fp = fopen("NpsPsm.tsv", 'r');

	while ( !feof($fp) )
	{
		$line = fgets($fp, 2048);

		$data = str_getcsv($line, $delimiter);

		if($data[0] == $titleid)
		{
			return($data[3]);
		}
	}                              

	fclose($fp);
}

?>
