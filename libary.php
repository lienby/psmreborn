<?php
include("header.php");
?>

<?php
function printGenreList(SimpleXMLElement $xml)
{
	$content = "";
	$xcount = count($xml);
	for ($x = 0; $x < $xcount; $x++) {
		$genre = $xml[$x]->attributes()->value;
		$content = $content.$genre;
		if($xcount > 1 && $x+1 < $xcount)
		{
			$content = $content.", ";
		}
	} 
	return $content;	
}

function printLanguageList(SimpleXMLElement $xml)
{
	$content = "";
	$xcount = count($xml);
	for ($x = 0; $x < $xcount; $x++) {
		$language = $xml[$x]->attributes()->locale;
		$content = $content.$language;
		if($xcount > 1 && $x+1 < $xcount)
		{
			$content = $content.", ";
		}
	} 
	return $content;	
}

function getSdkType(SimpleXMLElement $xml)
{
	if(isset($xml->app_xml_format))
	{
		if($xml->app_xml_format->attributes()->sdk_type == "PSM SDK")
		{
			return "PSM";
		}
		else
		{
			return "PSM Unity";
		}
	}
	else
	{
		return "PSM";
	}
}

if(isset($_GET["game"]))
{
	$game = htmlspecialchars($_GET["game"], ENT_QUOTES);
	
	$game = str_replace("/", "",$game);
	$game = str_replace(".", "",$game);
	$game = str_replace("*", "",$game);
	
	if(strlen($game) != 9)
	{
		echo("I like to see girls die,<br>This is not the bug your looking for :P");
		die();
	}
	echo("<script src=\"/zrif.js\"></script>");
	echo("<a id=\"zrif-bin-worker\" href=\"\"></a>");
	
	$title = getTitle($game);
	
	$xml = simplexml_load_file('gameinfo/' . $game . "/app.xml", 'SimpleXMLElement', LIBXML_NOENT);
	$genreList = $xml->genre_list->children();
	$languageList = $xml->name->children();
	$featureList = $xml->feature_list->children();
	$SDKType = getSdkType($xml); 
	$zrifinfo = getZRIF($game);
	$zrifPendingInfo = getPendingZRIF($game);
	$pkgSony = getPKG($game);
	$simulatorZip = "decrypted-files/".$game.".zip";
	
	$isplayable = "";

	if($zrifinfo == "MISSING" && $zrifPendingInfo == "MISSING")
	{
		$isplayable = "<a href=\"/faq.php#add-zrif\" class=\"white\">No, Missing zRIF - Do you have a working copy of this game?</a>";
	}	
	if(file_exists("psdp-packages/".$game.".psdp"))
	{
		$isplayable = "<a href=\"/psdp-packages/".$game.".psdp\" class=\"white\">Yes, with PSDP Package in Developer Assistant</a>";
	}
	if($zrifinfo != "MISSING")
	{
		$isplayable = "<a class=\"white\" onclick=\"copy_text('".$zrifinfo."')\">Yes! Copy zRIF String</a> or <a class=\"white\" onclick=\"convert2('".$zrifinfo."')\">Download FAKE.RIF</a>";
	}
	if($zrifPendingInfo != "MISSING")
	{
		$isplayable = "<a class=\"white\" onclick=\"copy_text('".$zrifPendingInfo."')\">Maybe, Ulthough it hasnt been tested yet- Copy zRIF String</a> or <a class=\"white\" onclick=\"convert2('".$zrifPendingInfo."')\">Download FAKE.RIF</a>";		
	}
	
	echo'<div id="bg-content" style="width:100%; height:100%; background-image: url(\'/gameinfo/'.$game.'/splash_854x480.png\'); background-repeat: no-repeat; background-size: cover;height: 100%; background-size: cover;");>
			<div class="psm-meta">
				<img src="/gameinfo/'. $game .'/icon_256x256.png" id="psm-icon" width="256" height="256">
					<div id="psm-title">
						'. $title .' ('.$game.')
					</div>
				</img>
				
				<div id="psm-infos">
					<ul>
						<li><p>Playable: '.$isplayable.'</p></li>
						<li><p>Metadata: <a href="/gameinfo/'.$game.'/app.xml" class="white">app.xml</a>, <a href="/gameinfo/'.$game.'/text.txt" class="white">copyright.txt</a></p></li>
						<li><p>Images: <a href="/gameinfo/'.$game.'/icon_128x128.png" class="white">icon_128x128.png</a>, <a href="/gameinfo/'.$game.'/icon_256x256.png" class="white">icon_256x256.png</a>, <a href="/gameinfo/'.$game.'/icon_512x512.png" class="white">icon_512x512.png</a>, <a href="/gameinfo/'.$game.'/splash_854x480.png" class="white">splash_854x480.png</a></p></li>
						<li><p>Supported locale: <b>'.printLanguageList($languageList).'</b></p></li>
						<li><p>Genres: <b>'.printGenreList($genreList).'</b></p></li>
						<li><p>Type: <b>'.$SDKType.'</b></p></li>
					</ul>
				</div>
				
				<div id="psm-downloads">
					<select onchange="pkg_change_version()" class="version-select">
					  ';
					  $dirlist = glob("pkg/".$game."_00/*");
					  $vercount = count($dirlist);
					  for($i = 0; $i < $vercount; $i++)
					  {
						$dirname = basename($dirlist[$i]);
						
						if($i == $vercount - 1)
						{
							echo'<option selected="'.$dirname.'" value="'.$dirname.'">'.$dirname.'</option>';
							$pkgReborn = "/pkg/".$game."_00/".$dirname."/".$game."_00.pkg";
						}
						else
						{
							echo'<option value="'.$dirname.'">'.$dirname.'</option>';
						}
					  }
					  
					  echo'
					</select>
					<h1>Downloads: </h1>
					
					<div class="button-enabled" id="pkg-sony" onclick="open_url(\''.$pkgSony.'\')">
						<a href="'.$pkgSony.'" class="nostyle">PKG (Sony Server)</a>
					</div>
					
					<div class="button-enabled" id="pkg-psmrb" onclick="open_url(\''.$pkgReborn.'\')">
						<a href="'.$pkgReborn.'" class="nostyle">PKG (PSMReborn Server)</a>
					</div>
					
					<div class="';
					if(file_exists($simulatorZip))
					{
						echo 'button-enabled" onclick="open_url(\''.$simulatorZip.'\')">	
						<a href="'.$simulatorZip.'" class="nostyle">Decrypted Files (For Simulator)</a>';
					}
					else
					{
						echo 'button-disabled">	
						Decrypted Files (For Simulator)';
					}
					echo '</div>
				</div>
			</div>
		</div>';
}
else
{
	$searchUsed = isset($_GET["search"]);
	
	$simulatorSearch = false;
	$unplayableSearch = false;
	$playableSearch = false;
	
	if(isset($_GET["simulator"]))
	{
		if($_GET["simulator"] == "on")
			$simulatorSearch = true;
	}
	
	if(isset($_GET["unplayable"]))
	{
		if($_GET["unplayable"] == "on")
			$unplayableSearch = true;
	}
	
	if(isset($_GET["playable"]))
	{
		if($_GET["playable"] == "on")
			$playableSearch = true;
	}
			
	echo'<div id="search-form">
			<form action="" method="get">
				<input type="text" name="search" value="';
				if(isset($_GET["search"]))
				{
					echo htmlspecialchars($_GET["search"], ENT_QUOTES);
				}
				echo '"></input>
				<input type="submit"></input><br>
				<input type="radio" name="searchby" value="title" ';
				if(!$searchUsed || isset($_GET["searchby"]))
				{
					if(!$searchUsed || $_GET["searchby"] == "title" )
					{
						echo 'checked="checked"';
					}
				}
				echo '>Title</input>
				<input type="radio" name="searchby" value="titleid" ';
				if(isset($_GET["searchby"]))
				{
					if($_GET["searchby"] == "titleid")
					{
						echo 'checked="checked"';
					}
				}
				echo '>Title ID</input>
				<input type="radio" name="searchby" value="dev" ';
				if(isset($_GET["searchby"]))
				{
					if($_GET["searchby"] == "dev")
					{
						echo 'checked="checked"';
					}
				}
				echo '>Developer</input><br>
				<input type="checkbox" name="playable" ';
				if(!$searchUsed || $playableSearch)
				{
					echo 'checked="checked"';
				}
				echo'>Show Playable (Has zRIF)</input>
				<input type="checkbox" name="simulator" ';
				if(!$searchUsed || $simulatorSearch)
				{
					echo 'checked="checked"';
				}
				echo '>Show Simulator-Ready</input>
				<input type="checkbox" name="unplayable" ';
				if(!$searchUsed || $unplayableSearch)
				{
					echo 'checked="checked"';
				}
				echo '>Show Unplayable (Only PKG)</input>
			</form>
		</div>
	';
	
	
	echo('<div id="psm-gamelist">');
	$dirlist = glob("gameinfo/*");
	$playableList = getPlayableList();
	$playablePendingList = getPendingPlayableList();
	foreach ($dirlist as &$path) {
		$dirname = basename($path);
		$xml = simplexml_load_file($path . "/app.xml", 'SimpleXMLElement', LIBXML_NOENT);
		
		$title = $xml->name->localized_item[0]->attributes()->value;
		$version = $xml->attributes()->version;
		$genre = $xml->genre_list->genre->attributes()->value;
		$author = $xml->developer->name->attributes()->value;
		$website = $xml->website->attributes()->href;
		$isPlayable = $playableList[$dirname] == 1 || file_exists("psdp-packages/".$dirname.".psdp");
		$hasSimulator = file_exists("decrypted-files/".$dirname.".zip");
		$isPlayablePending = $playablePendingList[$dirname] == 1;
		
		if($searchUsed)
		{
			$search = $_GET["search"];
			
			
			if(isset($_GET["searchby"]) && $search !== "")
			{
				$searchby = $_GET["searchby"];
				if($searchby === "title")
				{
					if(strpos(strtolower($title), strtolower($search)) === false)
					{
						continue;
					}
				}
				elseif($searchby === "titleid")
				{
					if(strpos(strtolower($dirname), strtolower($search)) === false)
					{
						continue;
					}
				}
				elseif($searchby === "dev")
				{
					if(strpos(strtolower($author), strtolower($search)) === false)
					{
						continue;
					}
				}
			}
			
			
			if(!$simulatorSearch)
			{
				if($hasSimulator)
				{
					continue;
				}
			}
			
			if(!$unplayableSearch)
			{
				if(!$isPlayable && !$isPlayablePending)
				{
					continue;
				}
			}
			
			if(!$playableSearch)
			{
				if(($isPlayable || $isPlayablePending) && !$hasSimulator)
				{
					continue;
				}
			}
		}

		
		echo '<div class="psm-game" onclick="open_url(\'?game='.$dirname.'\')"">
				<a href="?game='.$dirname.'" class="nostyle">
					<img src="/gameinfo/'. $dirname .'/icon_128x128.png" width="124" height="124"> 
					';
					
					if(getSdkType($xml) == "PSM Unity")
					{
						echo '<img src="/img/unity.png" title="Made With Unity" width="10" height="10" >';
					}
					if($isPlayable)
					{
						echo '<img src="/img/playable.png" title="zRIF Known" width="10" height="10" >';
					}
					if($isPlayablePending)
					{
						echo '<img src="/img/pending_playable.png" title="zRIF Maybe Known" width="10" height="10" >';
					}
					if($hasSimulator)
					{
						echo '<img src="/img/simulator.png" title="Simulator Files Avalible" width="10" height="10" >';
					}
					
					$authorUrl = "libary.php?search=".urlencode($author)."&searchby=dev";
					if($playableSearch === true || $searchUsed === false)
						$authorUrl .= "&playable=on";
					if($simulatorSearch === true || $searchUsed === false)
						$authorUrl .= "&simulator=on";
					if($unplayableSearch === true || $searchUsed === false)
						$authorUrl .= "&unplayable=on";
					
					echo '<span id="psm-info">
						<p>
								<b>'.$title.'</b><br>
								Genre: <b>'.$genre.'</b><br>
								Version: <b>'.$version.'</b><br>
								<b>Author: <a class="white" href="'.$authorUrl.'">'.$author.'</a><br>
								Website: <a class="white" href="'.$website.'">'.$website.'</a><br></b>
						</p>
					</span>
				</a>
			</div>';
	}
	echo('</div>');
}
?>

