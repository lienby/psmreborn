<?php
include("header.php");

function format_version(string $version)
{
	return substr($version,0,2) .".". substr($version,2,2);
}

function getName(string $file)
{
	if($file == "psm_tool_set_for_unity_installer_ver09806_20131125_1848_for_Unity_r22419")
	{
		return "ToolSet v9806";
	}
	elseif(strpos($file, "MakePsmGreatAgain") !== false)
	{
		return "MPGA ".substr($file,19,4);
	}
	elseif(strpos($file, 'PSMToolSetForUnity_') !== false)
	{
		return "Toolset v".substr($file,19,7);
	}
	elseif($file == "UnitySetup_update-4.3.4f1")
	{
		return "Unity v4.3.4f1";
	}
	elseif($file == "UnitySetup_update-4.3.7p3")
	{
		return "Unity v4.3.7p3";
	}
	elseif($file == "psmdevassistant")
	{
		return "Dev Assistant";
	}
	elseif($file == "PlaystationCertificates")
	{
		return "PS Certificates";
	}
	elseif($file == "UnityJapan2014-Introduction_to_Unity-for-PSM")
	{
		return "Intro 2 Unity r1";
	}
	elseif($file == "UnityJapan2014-Introduction_to_Unity-for-PSM2")
	{
		return "Intro 2 Unity r2";
	}
	elseif($file == "PSM Dev 01.03")
	{
		return "PSM Dev 01.03";
	}
	elseif(strpos($file, 'PSMPublishingUtility_') !== false)
	{
		return "PSMPU ".substr($file,21,6);
	}
	elseif($file == "MichaelFleischauer-PlaystationmobileDevelopmentCookbook-packtPublishing2013")
	{
		return "PSMCookbook";
	}
	elseif($file == "IP9100-PCSI00011_00-PSMRUNTIME000000")
	{
		return "Runtime 01.00";
	}
	elseif(strpos($file, "IP9100-PCSI00011_00-PSMRUNTIME000000-A") !== false)
	{
		return "Runtime ".format_version(substr($file,38,4));
	}
	elseif(strpos($file, "IP9100-PCSI00007_00-PSSUITEDEV000000-A") !== false)
	{
		return "PSM Dev ".format_version(substr($file,38,4));
	}
	elseif(strpos($file, 'UnitySetup') !== false)
	{
		return substr($file,strpos($file,"-for-Editor-")+12,12);
	}
	elseif(strpos($file, "IP9100-PCSI00009_00-UNITYDEV00000000-A") !== false)
	{
		return "Unity Dev ".format_version(substr($file,38,4));
	}
	elseif(strpos($file, "PCSI00007-99_999-99_00-") !== false)
	{
		return "PSMCP ".str_replace("_",".",substr($file,23,5));
	}
	elseif(strpos($file, "PCSI00009-99_999-99_00-") !== false)
	{
		return "UNITYCP ".str_replace("_",".",substr($file,23,5));
	}
	elseif(strpos($file, "PSM_SDK_") !== false)
	{
		return "SDK ".substr($file,8,7);
	}
	elseif(strpos($file, "PSSuiteSDK_") !== false)
	{
		return "SDK ".substr($file,11,7);
	}
	else
	{
		return $file;
	}
}

function getIcon(string $file)
{
	$ext = pathinfo($file, PATHINFO_EXTENSION);
	if($ext == "psdp")
	{
		return "/img/psdp.png";
	}
	elseif($ext == "pdf")
	{
		return "/img/docs.png";
	}
	elseif($ext == "exe" && strpos($file, 'UnitySetup') == false)
	{
		return "/img/sdk.png";
	}
	elseif($ext == "exe" && strpos($file, 'UnitySetup') !== false)
	{
		return "/img/unity_icon.png";
	}
	elseif($ext == "pkg")
	{
		return "/img/pkg.png";
	}
	elseif($ext == "zip" || $ext == "tgz" || $ext == "gz" || $ext == "7z")
	{
		return "/img/zip_icon.png";
	}
	elseif($ext == "cmbackup")
	{
		return "/img/cmbackup.png";
	}
	elseif($ext == "ppk")
	{
		return "/img/ppk_icon.png";
	}
	elseif($ext == "apk")
	{
		return "/img/apk-icon.png";
	}
	elseif($ext == "skprx")
	{
		return "/img/kernel-plugin.png";
	}
	elseif($ext == "suprx")
	{
		return "/img/user-plugin.png";
	}
	else
	{
		return "/img/logo.png";
	}
}

?>

<div class="devtoollist">
	<?php
	$type = "";
	if(isset($_GET["type"]))
	{
		$type = htmlspecialchars($_GET["type"], ENT_QUOTES);
		
		$type = str_replace("/", "",$type);
		$type = str_replace(".", "",$type);
		$type = str_replace("*", "",$type);
		
		echo "<h1>".$type."/</h1>";
	}
	else
	{
		echo "<h1>What are you looking for?</h1>";
	}
	
	if($type == "")
	{
		echo'<div class="devtool" id="unity" onclick="open_url(\'?type=unity\')">
				<a href="?type=unity" class="image">
					<img src="/img/unity_icon.png" width="128" height="128" class="bubble">
					<span id="textContent">
						PSM Unity
					</span>
				</a>
			 </div>
		
			<div class="devtool" id="psm" onclick="open_url(\'?type=psm\')">
				<a href="?type=psm" class="image">
					<img src="/img/psm_icon.png" width="128" height="128" class="bubble">
					<span id="textContent">
						PSM
					</span>
				</a>
			</div>
		
			<div class="devtool" id="android" onclick="open_url(\'?type=psm-android\')">
				<a href="?type=psm-android" class="image">
					<img src="/img/androiddev.png" width="128" height="128" class="bubble">
					<span id="textContent">
						PSM Android
					</span>
				</a>
			</div>
			
			<div class="devtool" id="tools" onclick="open_url(\'?type=psm-tools\')">
				<a href="?type=psm-tools" class="image">
					<img src="/img/tools.png" width="128" height="128" class="bubble">
					<span id="textContent">
						Unofficial Tools
					</span>
				</a>
			</div>';
	}
	elseif($type == "psm")
	{
		echo'<div class="devtool" id="psmsdk" onclick="open_url(\'?type=psm-sdk\')">
				<a href="?type=psm-sdk" class="image">
					<img src="/img/sdk.png" width="128" height="128" class="bubble">
					<span id="textContent">
						PSM SDK
					</span>
				</a>
			 </div>
			 
			 <div class="devtool" id="psmruntime" onclick="open_url(\'?type=psm-runtime\')">
				<a href="?type=psm-runtime" class="image">
					<img src="/img/psmruntime.png" width="128" height="128" class="bubble">
					<span id="textContent">
						PSM Runtime
					</span>
				</a>
			 </div>
			 
			 <div class="devtool" id="psmdev" onclick="open_url(\'?type=psm-dev\')">
				<a href="?type=psm-dev" class="image">
					<img src="/img/psmdev.png" width="128" height="128" class="bubble">
					<span id="textContent">
						Dev  Assistant
					</span>
				</a>
			</div>
			
			<div class="devtool" id="psdp" onclick="open_url(\'?type=psm-psdp\')">
				<a href="?type=psm-psdp" class="image">
					<img src="/img/psdp.png" width="128" height="128" class="bubble">
					<span id="textContent">
						PSDP Packages
					</span>
				</a>
			</div>
			
			<div class="devtool" id="docs" onclick="open_url(\'?type=psm-docs\')">
				<a href="?type=psm-docs" class="image">
					<img src="/img/docs.png" width="128" height="128" class="bubble">
					<span id="textContent">
						Documentation
					</span>
				</a>
			</div>
			
			<div class="devtool" id="sources" onclick="open_url(\'?type=psm-sources\')">
				<a href="?type=psm-sources" class="image">
					<img src="/img/sources.png" width="128" height="128" class="bubble">
					<span id="textContent">
						PSM Sources
					</span>
				</a>
			</div>
			
			<div class="devtool" id="keys" onclick="open_url(\'?type=psm-offical-keys\')">
				<a href="?type=psm-offical-keys" class="image">
					<img src="/img/psmda_keys.png" width="128" height="128" class="bubble">
					<span id="textContent">
						Publisher Keys
					</span>
				</a>
			</div>
			
						
			<div class="devtool" id="drivers" onclick="open_url(\'?type=psm-drivers\')">
				<a href="?type=psm-drivers" class="image">
					<img src="/img/psm-drivers.png" width="128" height="128" class="bubble">
					<span id="textContent">
						PSM Drivers
					</span>
				</a>
			</div>
			'; 
	}
	elseif($type == "psm-sources")
	{
				echo'<div class="devtool" id="monosrc" onclick="open_url(\'?type=psm-mono-src\')">
						<a href="?type=psm-mono-src" class="image">
							<img src="/img/mono.png" width="128" height="128" class="bubble">
							<span id="textContent">
								PSM Mono Src
							</span>
						</a>
					</div>
					
					<div class="devtool" id="gamessrc" onclick="open_url(\'?type=psm-games-src\')">
						<a href="?type=psm-games-src" class="image">
							<img src="/img/games.png" width="128" height="128" class="bubble">
							<span id="textContent">
								PSM Games Src
							</span>
						</a>
					</div>
					';
	}
	elseif($type == "unity")
	{
		echo'<div class="devtool" id="unityexport" onclick="open_url(\'?type=unity-support\')">
				<a href="?type=unity-support" class="image">
					<img src="/img/export.png" width="128" height="128" class="bubble">
					<span id="textContent">
						Unity Exports
					</span>
				</a>
			</div>
			
			<div class="devtool" id="unitydev" onclick="open_url(\'?type=unity-dev\')">
				<a href="?type=unity-dev" class="image">
					<img src="/img/unitydev.png" width="128" height="128" class="bubble">
					<span id="textContent">
						Dev Assistant
					</span>
				</a>
			</div>
			
			<div class="devtool" id="unitydocs" onclick="open_url(\'?type=unity-docs\')">
				<a href="?type=unity-docs" class="image">
					<img src="/img/docs.png" width="128" height="128" class="bubble">
					<span id="textContent">
						Documentation
					</span>
				</a>
			</div>
			
			<div class="devtool" id="unityassets" onclick="open_url(\'?type=unity-assets\')">
				<a href="?type=unity-assets" class="image">
					<img src="/img/psm-drivers.png" width="128" height="128" class="bubble">
					<span id="textContent">
						Assets
					</span>
				</a>
			</div>
			
			<div class="devtool" id="unitytools" onclick="open_url(\'?type=unity-tools\')">
				<a href="?type=unity-tools" class="image">
					<img src="/img/tools.png" width="128" height="128" class="bubble">
					<span id="textContent">
						Tools for Unity
					</span>
				</a>
			</div>
			
			<div class="devtool" id="unity-psdp" onclick="open_url(\'?type=unity-psdp\')">
				<a href="?type=unity-psdp" class="image">
					<img src="/img/psdp.png" width="128" height="128" class="bubble">
					<span id="textContent">
						PSDP Packages
					</span>
				</a>
			</div>
		';
	}
	else
	{
		$dirlist = glob($type.'/*');
		foreach ($dirlist as &$path) {
			echo '<div class="devtool" onclick="open_url(\''.$path.'\')">
					<a href="'.$path.'" class="image" title="'.basename($path).'">
						<img src="'.getIcon($path).'" width="128" height="128" class="bubble">
						<span id="textContent">
							'.getName(pathinfo(basename($path), PATHINFO_FILENAME)).'
						</span>
					</a>
				 </div>';
		}
	}
	?>
</div>