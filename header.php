<?php include("common.php"); ?>
<head>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="common.js"></script>
	<?php
	$title = "";
	$description = "";
	$image = "";
	
	/*
	* Write title
	*/
	
	echo("<title>");
	$url = $_SERVER['REQUEST_URI'];
	if(strpos($url, 'libary.php') !== false)
	{
		if(isset($_GET["game"]))
		{
			$game = htmlspecialchars($_GET["game"], ENT_QUOTES);
			
			$game = str_replace("/", "",$game);
			$game = str_replace(".", "",$game);
			$game = str_replace("*", "",$game);
			
			$title = getTitle($game);
			$title.= " - ";
		}
		else
		{
			$title .= "PSM Game Library - ";
		}
	}
	else if(strpos($url, 'devtools.php') !== false)
	{
		$title .= "PlayStation Mobile Development Tools  - ";
		if(isset($_GET["type"]))
		{
			$type = htmlspecialchars($_GET["type"], ENT_QUOTES);
			$type = str_replace("/", "",$type);
			$type = str_replace(".", "",$type);
			$type = str_replace("*", "",$type);
			$title .= $type."/ - ";
		}
		
	}
	
	$title .= "PSM Reborn";
	
	echo($title);
	echo("</title>\n");
	
	/*
	* Write metadata
	*/
	echo('<meta charset="utf-8">');
	echo('<meta name="robots" content="all">');
	echo('<meta name="viewport" content="width=1000; user-scalable=0;"/>');
	echo('<meta name="Description" content="');
	if(strpos($url, 'libary.php') !== false)
	{
		if(isset($_GET["game"]))
		{
			$game = htmlspecialchars($_GET["game"], ENT_QUOTES);
			$game = str_replace("/", "",$game);
			$game = str_replace(".", "",$game);
			$game = str_replace("*", "",$game);
			$title = getTitle($game);
			$simulatorZip = "decrypted-files/".$game.".zip";
			$zrif = getZRIF($game);
			$pendingZrif = getPendingZRIF($game);
			
			$playable = (($zrif != "MISSING" || $pendingZrif) && file_exists("psdp-packages/".$game.".psdp") == false);
			
			$description .= "PlayStation Mobile PKG/ROM Download links for ".$title." (".$game." - ";
			if($playable)
			{
				$description .= "playable)";
			}
			else
			{
				$description .= "unplayable)";
			}
			$description .= " Download ".$game.".pkg, ";
			if(file_exists($simulatorZip))
			{
				$description .= $game.".zip (Simulator), ";
			}
			if(file_exists("psdp-packages/".$game.".psdp"))
			{
				$description .= $game.".psdp, ";
			}
			$description .= "now! - PSMReborn #1 place for all PSM Content";
			$image .= 'http://'.$_SERVER['HTTP_HOST'].'/gameinfo/'.$game.'/icon_512x512.png';
		}
		else
		{
			$description .= "PlayStation Mobile Game/App Library - PSMReborn #1 place for all PSM Content";
		}	
	}
	else if(strpos($url, 'devtools.php') !== false)
	{
		$description .= "PlayStation Mobile Development Tools";
		if(isset($_GET["type"]))
		{
			$type = htmlspecialchars($_GET["type"], ENT_QUOTES);
			$type = str_replace("/", "",$type);
			$type = str_replace(".", "",$type);
			$type = str_replace("*", "",$type);
			$description .= " - ".$type."/";
		}
		$description .= " - PSMReborn #1 place for all PSM Content";
	}
	else
	{
		echo("PSMReborn is the largest archive of PlayStation Mobile content on the internet containing Games, SDK's, Source Code, and Unity for PSM exporters.");
	}
	
	
	echo($description);
	echo('">');
	
	/*
	* Default Image
	*/
	if($image == "")
	{
		$image .= 'http://'.$_SERVER['HTTP_HOST'].'/img/logo.png';
	}
	/*
	* Social Media
	*/
	echo('<meta property="og:image" content="'.$image.'">');
	echo('<meta property="og:title" content="'.$title.'">');
	echo('<meta property="og:description" content="'.$description.'"');
	
	echo('<meta property="twitter:image" content="'.$image.'">');
	echo('<meta property="twitter:title" content="'.$title.'">');
	echo('<meta property="twitter:description" content="'.$description.'"');
	?>
</head>
<div class="header">
	<div class="sitename">
		<a href="/" class="image">
			<img src="img/logo.png" alt="PSMReborn" width="40" height="40">
		</a>
		PSMReborn
	</div>
	
	<div class="sitemap">
		<ul>
			<li><a href="/">Home</a></li>
			<li><a href="/libary.php">PSM Library</a></li>
			<li><a href="/devtools.php">Development Tools</a></li>
		</ul>
	</div>
</div>